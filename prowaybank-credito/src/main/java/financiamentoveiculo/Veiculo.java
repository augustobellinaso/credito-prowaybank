package financiamentoveiculo;

public class Veiculo {
	
	private Integer anoVeiculo;
	private Double valorVeiculo;
	private String marcaVeiculo;
	private TipoVeiculo tipoVeiculo;
	private SituacaoVeiculo situacaoVeiculo;
	
	public Veiculo(Integer anoVeiculo, Double valorVeiculo, String marcaVeiculo, TipoVeiculo tipoVeiculo,
			SituacaoVeiculo situacaoVeiculo) {
		this.anoVeiculo = anoVeiculo;
		this.valorVeiculo = valorVeiculo;
		this.marcaVeiculo = marcaVeiculo;
		this.tipoVeiculo = tipoVeiculo;
		this.situacaoVeiculo = situacaoVeiculo;
	}
	
	/**
	 * Retorna o ano do veiculo.
	 * @return
	 */
	public Integer getAnoVeiculo() {
		return anoVeiculo;
	}

	/**
	 * Altera o ano do veiculo.
	 * @param anoVeiculo
	 */
	public void setAnoVeiculo(Integer anoVeiculo) {
		this.anoVeiculo = anoVeiculo;
	}

	/**
	 * Retorna o valor do veiculo.
	 * @return
	 */	
	public Double getValorVeiculo() {
		return valorVeiculo;
	}

	/**
	 * Altera o valor do veiculo.
	 * @param valorVeiculo
	 */
	public void setValorVeiculo(Double valorVeiculo) {
		this.valorVeiculo = valorVeiculo;
	}

	/**
	 * Retorna a marca do veiculo. 
	 * @return
	 */
	public String getMarcaVeiculo() {
		return marcaVeiculo;
	}

	/**
	 * Altera a marca do veiculo.
	 * @param marcaVeiculo
	 */
	public void setMarcaVeiculo(String marcaVeiculo) {
		this.marcaVeiculo = marcaVeiculo;
	}

	/**
	 * Retorna o tipo do veiculo.
	 * @return
	 */
	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	/**
	 * Altera o tipo do veiculo.
	 * @param tipoVeiculo
	 */
	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	/**
	 * Retorna a situaiio do veiculo.
	 * @return
	 */
	public SituacaoVeiculo getSituacaoVeiculo() {
		return situacaoVeiculo;
	}

	/**
	 * Altera a situaiio do veiculo.
	 * @param situacaoVeiculo
	 */
	public void setSituacaoVeiculo(SituacaoVeiculo situacaoVeiculo) {
		this.situacaoVeiculo = situacaoVeiculo;
	}
	
}
