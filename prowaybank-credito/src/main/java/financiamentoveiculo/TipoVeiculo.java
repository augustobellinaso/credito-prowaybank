package financiamentoveiculo;

public enum TipoVeiculo {
    BARCO, CAMINHAO, CARRO, IATE, JETSKI, LANCHA, LIMOUSINE, MOTO, VAN
}
