package financiamentoveiculo;

public class FinanciamentoVeiculoController {

    FinanciamentoVeiculo financiamentoVeiculo;

    public FinanciamentoVeiculoController(FinanciamentoVeiculo financiamentoVeiculo) {
        this.financiamentoVeiculo = financiamentoVeiculo;
    }


    /**
     * Metodo que analisa se e possivel realizar o financiamento de um veiculo de
     * acordo com os dados fornecidos.
     *
     * @param tipoVeiculo
     * @param anoVeiculo
     * @param valorVeiculo
     * @param marcaVeiculo
     * @param situacaoVeiculo
     *
     */
    public String financiarVeiculo() {
        String resultado = "Pedido de financiamento aceito.";

        if ((this.financiamentoVeiculo.getScoreSerasa() < 301 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.MOTO && this.financiamentoVeiculo.getVeiculo().getSituacaoVeiculo() == SituacaoVeiculo.NOVO))
                || (this.financiamentoVeiculo.getScoreSerasa() < 701 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.IATE || this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.LIMOUSINE))) {
            resultado = "Nao e possivel realizar o financiamento do veiculo.";
        }

        return resultado;
    }

    /*
    * Método que concede desconto de acordo com o score do SERASA
    *
    *
    * */


    public String descontoFinanciarVeiculo() {
        String resultadoDesconto;
        Double valorVeiculo = null;

        if ((this.financiamentoVeiculo.getScoreSerasa() > 501 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.MOTO && this.financiamentoVeiculo.getVeiculo().getSituacaoVeiculo() == SituacaoVeiculo.NOVO))
                || (this.financiamentoVeiculo.getScoreSerasa() <= 701 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.CARRO))) {
            valorVeiculo = valorVeiculo - (15 / 100);
            resultadoDesconto = "Parabens! Seu financiamento foi aprovado com 15% de desconto";

        } else if ((this.financiamentoVeiculo.getScoreSerasa() > 701 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.MOTO && this.financiamentoVeiculo.getVeiculo().getSituacaoVeiculo() == SituacaoVeiculo.NOVO))
                || (this.financiamentoVeiculo.getScoreSerasa() <= 1000 && (this.financiamentoVeiculo.getVeiculo().getTipoVeiculo() == TipoVeiculo.CARRO))) {
            valorVeiculo = valorVeiculo - (25 / 100);
            resultadoDesconto = "Parabens! Seu financiamento foi aprovado com 25% de desconto";

        } else {
            resultadoDesconto = "Desculpe, mas não foi possível conceder um desconto na compra :(";
        }
            return resultadoDesconto;
    }

}
