package credito;

import parcela.Parcela;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Credito credito = new Credito(12, LocalDate.now());
        CreditoController controller = new CreditoController(credito);
        controller.calcularValorParcela(1.5, credito.getQuantidadeDeParcelas(), 30000.0);

        List<Parcela> parcelas = credito.getParcelas();
//        for (int i = 0; i < parcelas.size(); i++) {
//            System.out.println(parcelas.get(i));
//        }
        parcelas.forEach(p -> System.out.println(p));
//        parcelas.forEach(System.out::println);
    }
}
