package credito;

import parcela.Parcela;
import utils.FormatUtils;

import java.time.Period;
import java.util.List;

public class CreditoController {

    private Credito credito;

    private CreditoController() {
    }

    public CreditoController(Credito credito) {
        this.credito = credito;
    }


    public void calcularValorParcela(double taxaJuros, int qtdeParcelas, Double valorDesejado) {

        double valorParcelas;
        double jurosPorParcela;

        // Considerando @param taxaJuros ao m�s e parcelas fixas
        // https://mundoeducacao.uol.com.br/matematica/tabela-price.htm

        jurosPorParcela = ((Math.pow((taxaJuros / 100) + 1, qtdeParcelas) * (taxaJuros / 100))
                / (Math.pow((taxaJuros / 100) + 1, qtdeParcelas) - 1));

        valorParcelas = valorDesejado * jurosPorParcela;
        String valorFormatado = FormatUtils.formatDecimal(valorParcelas);
        valorParcelas = Double.parseDouble(valorFormatado);
        credito.gerarParcelas(valorParcelas);
    }

    /**
     * Calcula o valor da parcela em atraso
     *
     * @param parcela Recebe um objeto do tipo Parcela
     */
    public void valorParcelaEmAtraso(Parcela parcela) {
        int diasAtraso = Period.between(parcela.getDataVencimento(), parcela.getDataPagamento()).getDays();
        double taxaJuros = 0.03;
        double jurosAtraso = diasAtraso * taxaJuros;
        double valor = parcela.getValorParcela();
        double novoValor = (valor *= (1 + jurosAtraso));
        parcela.setValorParcela(novoValor);
    }

    public String pagarMensalidade(Parcela parcela, Double valorPagamento) {
        Double valorASerPago = parcela.getValorParcela();
        if (parcela.getDataPagamento().isAfter(parcela.getDataVencimento())) {
            valorParcelaEmAtraso(parcela);
            valorASerPago = parcela.getValorParcela();
            if (valorPagamento.equals(valorASerPago)) {
                credito.setValorSolicitado(credito.getValorSolicitado() - valorASerPago);
                credito.setQuantidadeDeParcelas(credito.getQuantidadeDeParcelas() - 1);
                return "Mensalidade em atraso paga";
            }
        }
        if (valorPagamento.equals(valorASerPago)) {
            credito.setValorSolicitado(credito.getValorSolicitado() - valorASerPago);
            credito.setQuantidadeDeParcelas(credito.getQuantidadeDeParcelas() - 1);
            return "Mensalidade paga";
        } else if (valorPagamento < valorASerPago) {
            return "Valor insuficiente";
        }
        return "O valor do pagamento é maior do que o valor da parcela, tente novamente mais tarde";
    }



    /**
     * Verifica se o crédito solicitado é menor ou igual ao limite de crédito disponível.
     *
     * @param solicitado valor do crédito solicitado
     * @return boolean - se aprovou ou não a soliticação com base no valor solicitado e o crédito disponível.
     */
    public boolean verificarCreditoSolicitado(double solicitado) {
        return solicitado <= credito.getCreditoDisponivel();
    }

    /**
     * Faz a aprovação do crédito. Se o score do cliente for menor que 300 não aprova, caso o score seja maior do que 300
     * e o valor solicitado for menor ou igual do valor de crédito disponível para solicitar, retorna verdadeiro
     * aprovando o crédito. Caso não cumpra nenhum dos requisitos, não faz a aprovação.
     *
     * @return boolean - resultado da aprovação ou não do crédito.
     */
    public boolean aprovarCredito() {
        if (credito.getScore() <= 300) {
            return false;
        } else if (credito.getScore() > 300 && this.verificarCreditoSolicitado(credito.getValorSolicitado())) {
            return true;
        }
        return false;
    }

    public Credito getCredito() {
        return credito;
    }

    public void setCredito(Credito credito) {
        this.credito = credito;
    }
}
