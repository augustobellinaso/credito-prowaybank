package credito;

import parcela.Parcela;
import utils.FormatUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Credito {

    private int score;
    private Double creditoDisponivel;
    private Double valorSolicitado;
    private int quantidadeDeParcelas;
    private LocalDate dataSolicitacao;
    private boolean statusCredito;
    private List<Parcela> parcelas = new ArrayList<>();

    public Credito() {
    }

    public Credito(int quantidadeDeParcelas, LocalDate dataSolicitacao) {
        this.quantidadeDeParcelas = quantidadeDeParcelas;
        this.dataSolicitacao = dataSolicitacao;
    }

    public void gerarParcelas(Double valorParcela) {
        for (int i = 1; i <= this.quantidadeDeParcelas; i++) {
            LocalDate dataVencimento = this.dataSolicitacao.plusMonths(i);
            Parcela parcela = new Parcela(dataVencimento, valorParcela);
            parcelas.add(parcela);
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Double getCreditoDisponivel() {
        return creditoDisponivel;
    }

    public void setCreditoDisponivel(double creditoDisponivel) {
        this.creditoDisponivel = creditoDisponivel;
    }

    public double getValorSolicitado() {
        return valorSolicitado;
    }

    public void setValorSolicitado(double valorSolicitado) {
        this.valorSolicitado = valorSolicitado;
    }

    public int getQuantidadeDeParcelas() {
        return quantidadeDeParcelas;
    }

    public void setQuantidadeDeParcelas(int quantidadeDeParcelas) {
        this.quantidadeDeParcelas = quantidadeDeParcelas;
    }

    public LocalDate getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(LocalDate dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public boolean isStatusCredito() {
        return statusCredito;
    }

    public void setStatusCredito(boolean statusCredito) {
        this.statusCredito = statusCredito;
    }

    public void setCreditoDisponivel(Double creditoDisponivel) {
        this.creditoDisponivel = creditoDisponivel;
    }

    public void setValorSolicitado(Double valorSolicitado) {
        this.valorSolicitado = valorSolicitado;
    }

    public List<Parcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<Parcela> parcelas) {
        this.parcelas = parcelas;
    }
}
