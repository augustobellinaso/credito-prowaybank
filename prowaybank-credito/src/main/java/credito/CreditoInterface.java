package credito;

import java.time.LocalDate;

public interface CreditoInterface {
    double calcularValorParcela(double taxaJuros, int qtdeParcelas, Double valorDesejado);
    boolean verificarCreditoSolicitado(double solicitado);
    boolean aprovarCredito();
    double valorParcelaEmAtraso(LocalDate dataVencimento, LocalDate dataPagamento, double valorParcela);
    String pagarMensalidade(Double parcela, LocalDate diaDoPagamento);

}
