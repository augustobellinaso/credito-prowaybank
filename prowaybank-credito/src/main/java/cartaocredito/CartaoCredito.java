package cartaocredito;

public class CartaoCredito {
    String nomeCliente, emailCliente, cpfCliente, celularCliente;
    int scoreCliente;
    double rendaMensalCliente;

    /**
     * Método para adicionar/obter o nome do(a) cliente
     */
    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        if (nomeCliente.length() <=3 || nomeCliente.length() >=70) {
            throw new IllegalArgumentException("Por gentileza, verificar nome digitado");
        }
        this.nomeCliente = nomeCliente;
    }

    /**
     * Método para adicionar/obter o e-mail do(a) cliente
     */
    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        if (emailCliente == null) {
            throw new IllegalArgumentException("Campo inválido");
        }
        this.emailCliente = emailCliente;
    }

    /**
     * Método para adicionar/obter o CPF do(a) cliente
     */
    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        if (cpfCliente == null) {
            throw new IllegalArgumentException("Campo inválido");
        }
        this.cpfCliente = cpfCliente;
    }

    /**
     * Método para adicionar/obter o celular do(a) cliente
     */
    public String getCelularCliente() {
        return celularCliente;
    }

    public void setCelularCliente(String celularCliente) {
        if (celularCliente == null) {
            throw new IllegalArgumentException("Campo inválido");
        }
        this.celularCliente = celularCliente;
    }

    /**
     * Método para adicionar/obter a renda mensal do(a) cliente
     */
    public double getRendaMensalCliente() {
        return rendaMensalCliente;
    }

    public void setRendaMensalCliente(double rendaMensalCliente) {
        if (rendaMensalCliente <0) {
            throw new IllegalArgumentException("Campo inválido");
        }
        this.rendaMensalCliente = rendaMensalCliente;
    }

    /**
     * Método para adicionar/obter o score do(a) cliente
     */
    public int getScoreCliente() {
        return scoreCliente;
    }

    public void setScoreCliente(int scoreCliente) {
        if (scoreCliente <0) {
            throw new IllegalArgumentException("Campo inválido");
        }
        this.scoreCliente = scoreCliente;
    }
}