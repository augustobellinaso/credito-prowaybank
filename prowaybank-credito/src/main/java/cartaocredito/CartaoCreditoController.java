package cartaocredito;

public class CartaoCreditoController {

    /* Instância da classe cartaoCredito */
    CartaoCredito cartaoCredito;


    /*
     *
     * Metodo que aprova a proposta do cartao de credito solicitada.
     * O requisito para aprovacao é ter score maior que 500 e renda maior que R$2.400
     * Retorno do método deve ser true para aprovação da proposta.
     *
     * (Tabela Serasa Score 2.0)
     * Muito bom: 701 - 1000
     * Bom: 501 - 700
     * Regular: 301 - 500
     * Baixo: 0 - 300
     *
     * */

    public boolean validarPropostaCartaoCrCliente(){
        if (cartaoCredito.getScoreCliente() <=500 && cartaoCredito.getRendaMensalCliente() <=2400.00) {
            return false;
        } else {
            return true;
        }
    }
}