package renegociacao; // model

/**
 * Class  para renegocia��o de dividas com suas variaveis
 *
 * @author Cleber Santos
 */
public class RenegociarDivida {
    private Double valorDivida;
    private Double taxaJuros;
    private Integer parcelas;

    /**
     * Parametros com seus respectios return
     *
     * @param valorDivida
     * @param taxaJuros
     * @param parcelas
     * @author Cleber Santos
     */
    public RenegociarDivida(Double valorDivida, Double taxaJuros, Integer parcelas) {

        this.valorDivida = valorDivida;
        this.taxaJuros = taxaJuros;
        this.parcelas = parcelas;
    }

    public Double getValorDivida() {           // get busca o valor
        return valorDivida;
    }

    public void setValorDivida(Double valorDivida) {        // set altera o valor
        this.valorDivida = valorDivida;
    }

    public Double getTaxaJuros() {
        return taxaJuros;
    }

    public void setTaxaJuros(Double taxaJuros) {
        this.taxaJuros = taxaJuros;
    }

    public Integer getParcelas() {
        return parcelas;
    }

    public void setParcelas(Integer parcelas) {
        this.parcelas = parcelas;
    }


}
