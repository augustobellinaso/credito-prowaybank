package renegociacao;

/**
 * Classes onde est�o contidos os valores para renegociar.
 *
 * @author Cleber Santos
 */
public class renegociacao {
    /**
     * M�todo  com variaveis  que contem informa��es das parcelas, juros e o total.
     *
     * @param meses
     */
    public Double valorMensal(double valorDivida, int meses) { /* parametros */

        double parcelasDivida = valorDivida / meses; /* variaveis */
        double juros = 5.0 / 100.0;
        double total = 0;
        /**
         * If pega o m�s ( maior que 1 ) entra no FOR para fazer a condi��o de adicionar meses, total recebe as parcelas * os juros, pegando o juros e
         acrescentando o valor da parcela.
         *
         *@author Cleber Santos
         */
        if (meses > 1) {
            for (int i = 0; i < meses; i++) {
                total += parcelasDivida * juros + parcelasDivida;   // formato statico.
            }
            /**
             * Se meses for igual a 1, receber� desconto de 5%.
             *
             * @author Cleber Santos
             *
             */
        } else if (meses == 1) {
            total = parcelasDivida * 0.95;
        }
        return total;


    }

}