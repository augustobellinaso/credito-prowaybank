package parcela;

import java.time.LocalDate;

public class Parcela {

    private LocalDate dataVencimento;
    private LocalDate dataPagamento;
    private Double valorParcela;

    public Parcela() {
    }

    public Parcela(LocalDate dataVencimento, Double valorParcela) {
        this.dataVencimento = dataVencimento;
        this.valorParcela = valorParcela;
    }

    public Parcela(LocalDate dataVencimento, LocalDate dataPagamento, Double valorParcela) {
        this.dataVencimento = dataVencimento;
        this.dataPagamento = dataPagamento;
        this.valorParcela = valorParcela;
    }

    public LocalDate getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(LocalDate dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public LocalDate getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(LocalDate dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public Double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(Double valorParcela) {
        this.valorParcela = valorParcela;
    }

    @Override
    public String toString() {
        return "Parcela{" +
                "dataVencimento=" + dataVencimento +
                ", valorParcela=" + valorParcela +
                '}';
    }
}
