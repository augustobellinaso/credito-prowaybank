package creditoimobiliario;

import credito.Credito;

import java.util.LinkedList;
import java.util.List;

public class CreditoHistorico extends Credito {

    private String tipoDoImovel, localizacaoUF, aprovado;
    private int mesesAdiquirir, prazoEmMeses;
    private double valorDoImovel, valorFinanciado, rendaMensal, juros;
    CreditoHistorico[] historicoVect = new CreditoHistorico[1];
    List<Double> parcelasFinanciamento = new LinkedList<>();



    /**
     * Contrutor padrão
     */
    public CreditoHistorico() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Construtor com parametros
     *
     * @param tipoDoImovel
     * @param localizacaoUF
     * @param mesesAdiquirir
     * @param prazoEmMeses
     * @param valorDoImovel
     * @param valorFinanciado
     * @param rendaMensal     return Metodo contrutor
     */
    public CreditoHistorico(String tipoDoImovel, String localizacaoUF, String aprovado, int mesesAdiquirir, int prazoEmMeses,
                            double valorDoImovel, double valorFinanciado, double rendaMensal) {
        this.tipoDoImovel = tipoDoImovel;
        this.localizacaoUF = localizacaoUF;
        this.aprovado = aprovado;
        this.mesesAdiquirir = mesesAdiquirir;
        this.prazoEmMeses = prazoEmMeses;
        this.valorDoImovel = valorDoImovel;
        this.valorFinanciado = valorFinanciado;
        this.rendaMensal = rendaMensal;
    }

    /**
     * @return metodos especiais
     */
    public List<Double> getParcelasFinanciamento() {
        return parcelasFinanciamento;
    }
    public void setParcelasFinanciamento(List<Double> parcelasFinanciamento) {
        this.parcelasFinanciamento = parcelasFinanciamento;
    }

    public String getTipoDoImovel() {
        return tipoDoImovel;
    }
    public void setTipoDoImovel(String tipoDoImovel) {
        this.tipoDoImovel = tipoDoImovel;
    }

    public String getLocalizacaoUF() {
        return localizacaoUF;
    }
    public void setLocalizacaoUF(String localizacaoUF) {
        this.localizacaoUF = localizacaoUF;
    }

    public int getMesesAdiquirir() {
        return mesesAdiquirir;
    }
    public void setMesesAdiquirir(int mesesAdiquirir) {
        this.mesesAdiquirir = mesesAdiquirir;
    }

    public int getPrazoEmMeses() {
        return prazoEmMeses;
    }
    public void setPrazoEmMeses(int prazoEmMeses) {
        this.prazoEmMeses = prazoEmMeses;
    }

    public double getValorDoImovel() {
        return valorDoImovel;
    }
    public void setValorDoImovel(double valorDoImovel) {
        this.valorDoImovel = valorDoImovel;
    }

    public double getValorFinanciado() {
        return valorFinanciado;
    }
    public void setValorFinanciado(double valorFinanciado) {
        this.valorFinanciado = valorFinanciado;
    }

    public double getRendaMensal() {
        return rendaMensal;
    }
    public void setRendaMensal(double rendaMensal) {
        this.rendaMensal = rendaMensal;
    }

    public String getAprovado() {
        return aprovado;
    }
    public void setAprovado(String aprovado) {
        this.aprovado = aprovado;
    }

    public double getJuros() {
        return juros;
    }
    public void setJuros(double juros) {
        this.juros = juros;
    }

    @Override
    public String toString() {
        return "CreditoHistorico [tipoDoImovel=" + tipoDoImovel + ", localizacaoUF=" + localizacaoUF
                + ", mesesAdiquirir=" + mesesAdiquirir + ", prazoEmMeses=" + prazoEmMeses + ", valorDoImovel="
                + valorDoImovel + ", valorFinanciado=" + valorFinanciado + ", rendaMensal=" + rendaMensal + "]";
    }

}
