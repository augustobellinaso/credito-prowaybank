package creditoimobiliario;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CreditoHistoricoController {
    CreditoHistorico credito;


    public CreditoHistoricoController(CreditoHistorico credito) {
        this.credito = credito;
    }

    /**
     * @param historicoVect
     * @return O metodo usa um loop para percorrer todos o objetos da array de
     * historico de solicitacoes de credito, e invoca os metodos toString() e
     * o aprovar() da classe CreditoHistorico, exibindo na tela todos os itens
     * da array de dizendo se suas solicitacoes de creditos foram aprovadas
     * ou nao.
     */
    public String[][] showAllThatSweetInformation(CreditoHistorico historicoVect[]) {
        String[] toString = new String[historicoVect.length];
        String[] aprovar = new String[historicoVect.length];

        for (int i = 0; i < historicoVect.length; i++) {

            toString[i] = historicoVect[i].toString();
            aprovar[i] = historicoVect[i].getAprovado();

        }
        String[][] matrix = {toString, aprovar};
        return matrix;
    }

    /**
     * @return Metodo que compara o valor do imovel em relacao ao valor a ser
     * financiado e ao salario da pessoa, e retorna se o credito foi aprovado
     * ou nao
     */
    public String aprovar(CreditoHistorico credito) {
        if (credito.getValorDoImovel() >= credito.getValorFinanciado()
                && credito.getRendaMensal() >= (credito.getValorDoImovel() / 50)) {
            credito.setAprovado("Crédito aprovado");
            return credito.getAprovado();
        } else {
            credito.setAprovado("Crédito reprovado");
            return credito.getAprovado();
        }
    }

    /**
     * Metodo que formata uma data para o padrao brasileiro
     * @param data
     * @return retorna a data formatada em formato de String dd/MM/yyyy
     */
    public String formatter(LocalDate data) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return data.format(formatter);
    }

    /**
     * Metodo que retorna a data atual
     * @return LocalDate
     */
    public LocalDate dataAtual() {
        LocalDate dataAtual = LocalDate.now();
        return LocalDate.parse(formatter(dataAtual));
    }

    /**
     * retorna a data do ultimo pagamento
     * @param monthsQuantity
     * @return retorna a data do ultimo pagamento
     */
    public LocalDate dataDoUltimoPagamento(int monthsQuantity) {
        LocalDate ultimoPagamento = dataAtual().plusMonths(monthsQuantity);
        return ultimoPagamento;
    }
}
