package credito;

import org.junit.jupiter.api.Test;
import parcela.Parcela;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class CreditoTest {

    @Test
    public void deveRetornarTrueSeCreditoSolicitadoMenorQueCreditoDisponivel() {
        Credito credito = new Credito();
        credito.setCreditoDisponivel(10000.0);
        CreditoController controller = new CreditoController(credito);
        boolean aprovar = controller.verificarCreditoSolicitado(5000.0);
        assertTrue(aprovar);
    }

    @Test
    public void deveRetornarTrueSeCreditoSolicitadoIgualAoCreditoDisponivel() {
        Credito credito = new Credito();
        credito.setCreditoDisponivel(10000.0);
        CreditoController controller = new CreditoController(credito);
        boolean aprovar = controller.verificarCreditoSolicitado(5000.0);
        assertTrue(aprovar);
    }

    @Test
    public void deveRetornarFalseSeCreditoSolicitadoMaiorQueCreditoDisponivel() {
        Credito credito = new Credito();
        credito.setCreditoDisponivel(10000.0);
        CreditoController controller = new CreditoController(credito);
        boolean aprovar = controller.verificarCreditoSolicitado(15000.0);
        assertFalse(aprovar);
    }

    @Test
    public void deveRetornarFalseSeScoreMenorQue300() {
        Credito credito = new Credito();
        credito.setScore(200);
        CreditoController controller = new CreditoController(credito);
        boolean resultado = controller.aprovarCredito();
        assertFalse(resultado);
    }

    @Test
    public void deveRetornarTrueSeScoreMaiorQue300ECreditoAprovado() {
        Credito credito = new Credito();
        credito.setScore(400);
        credito.setCreditoDisponivel(1000.0);
        credito.setValorSolicitado(500);
        CreditoController controller = new CreditoController(credito);
        boolean resultado = controller.aprovarCredito();
        assertTrue(resultado);
    }

    @Test
    public void deveRetornarFalseSeScoreMaiorQue300ECreditoReprovado() {
        Credito credito = new Credito();
        credito.setScore(400);
        credito.setCreditoDisponivel(1000.0);
        credito.setValorSolicitado(1500);
        CreditoController controller = new CreditoController(credito);
        boolean resultado = controller.aprovarCredito();
        assertFalse(resultado);
    }

//    @Test
//    public void deveAcrescenter3PorCentoJurosSeAtrasarUmDia() {
//        Credito credito = new Credito();
//        Parcela parcela = new Parcela(LocalDate.now(), LocalDate.now().plusDays(1), 100.0);
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.valorParcelaEmAtraso(parcela);
//        double valorPagar = parcela.getValorParcela();
//        assertEquals(103.0, valorPagar);
//    }
//
//    @Test
//    public void deveAcrescenter30PorCentoJurosSeAtrasar10Dias() {
//        Credito credito = new Credito();
//        Parcela parcela = new Parcela(LocalDate.now(), LocalDate.now().plusDays(10), 100.0);
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.valorParcelaEmAtraso(parcela);
//        double valorPagar = parcela.getValorParcela();
//        assertEquals(130.0, valorPagar);
//    }
//
//    @Test
//    public void valorDaParcelaParaEmprestimoDe30000() {
//        Credito credito = new Credito();
//        Parcela parcela = new Parcela();
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.calcularValorParcela(1.5, 12, 30000.0);
//        double valorParcela = parcela.getValorParcela();
//        assertEquals(2750.4, valorParcela);
//    }
//
//    @Test
//    public void deveRetornarMensalidadePagaSePagaAteVencimento() {
//        Credito credito = new Credito();
//        credito.setValorSolicitado(30000.0);
//        Parcela parcela = new Parcela();
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.calcularValorParcela(1.5, 12, 30000.0);
//        parcela.setDataVencimento(LocalDate.now());
//        parcela.setDataPagamento(LocalDate.now());
//        String msg = controller.pagarMensalidade(parcela, 2750.4);
//        assertEquals("Mensalidade paga", msg);
//    }
//
//    @Test
//    public void deveRetornarMensalidadeEmAtrasoPagaSePagaAposVencimento() {
//        Credito credito = new Credito();
//        credito.setValorSolicitado(30000.0);
//        Parcela parcela = new Parcela();
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.calcularValorParcela(1.5, 12, 30000.0);
//        parcela.setDataVencimento(LocalDate.now());
//        parcela.setDataPagamento(LocalDate.now().plusDays(1));
//        String msg = controller.pagarMensalidade(parcela, 2750.4 * 1.03);
//        assertEquals("Mensalidade em atraso paga", msg);
//    }
//
//    @Test
//    public void deveRetornarValorInsuficienteSeInformarValorMenorQueParcela() {
//        Credito credito = new Credito();
//        credito.setValorSolicitado(30000.0);
//        Parcela parcela = new Parcela();
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.calcularValorParcela(1.5, 12, 30000.0);
//        parcela.setDataVencimento(LocalDate.now());
//        parcela.setDataPagamento(LocalDate.now().plusDays(10));
//        String msg = controller.pagarMensalidade(parcela, 2750.4);
//        assertEquals("Valor insuficiente", msg);
//    }
//
//    @Test
//    public void deveInformarQueOValorEMaiorQueODaParcela() {
//        Credito credito = new Credito();
//        credito.setValorSolicitado(30000.0);
//        Parcela parcela = new Parcela();
//        CreditoController controller = new CreditoController(credito, parcela);
//        controller.calcularValorParcela(1.5, 12, 30000.0);
//        parcela.setDataVencimento(LocalDate.now());
//        parcela.setDataPagamento(LocalDate.now());
//        String msg = controller.pagarMensalidade(parcela, 3500.0);
//        assertEquals("O valor do pagamento é maior do que o valor da parcela, tente novamente mais tarde", msg);
//    }

}