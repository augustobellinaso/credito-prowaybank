package creditopessoal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreditoPessoalControllerTest {

	@Test
	void creditoPessoalControllerTest() {
		CreditoPessoal credito = new CreditoPessoal(25000.0, 10, 7.5);
		CreditoPessoalController solicitacao = new CreditoPessoalController(credito);
		double expected = 3642.15;
		double result = solicitacao.solicitarCreditoPessoal();

		assertEquals(expected, result, 0.00001);
	}

	@Test
	void corrigirLimiteDeCreditoParaScorreMaiorQue700() {
		CreditoPessoal limite = new CreditoPessoal(25000.0, 10, 7.5);
		CreditoPessoalController solicitacao = new CreditoPessoalController(limite);
		limite.setCreditoDisponivel(1000);
		limite.setScore(750);
		solicitacao.corrigirLimiteCredito();
		double novoLimiteDeCredito = limite.getCreditoDisponivel();
		assertEquals(1500, novoLimiteDeCredito, 0.00001);

	}

	@Test
	void corrigirLimiteDeCreditoParaScorreMaiorQue900() {
		CreditoPessoal limite = new CreditoPessoal(25000.0, 10, 7.5);
		CreditoPessoalController solicitacao = new CreditoPessoalController(limite);
		limite.setCreditoDisponivel(1000);
		limite.setScore(950);
		solicitacao.corrigirLimiteCredito();
		double novoLimiteDeCredito = limite.getCreditoDisponivel();
		assertEquals(3000, novoLimiteDeCredito, 0.00001);

	}
	

}
